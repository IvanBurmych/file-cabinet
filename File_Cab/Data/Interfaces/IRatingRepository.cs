﻿using File_Cab.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Repositories.Interfaces
{
    public interface IRatingRepository : IRepository<Rating>
    {
        IQueryable<Rating> GetAllWithTextAndAuthorAsync();
        Task<Rating> GetWithTextAndAuthorByIdAsync(int id);
    }
}
