﻿using File_Cab.Data;
using File_Cab.Repositories;
using File_Cab.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext Context;
        public UnitOfWork(DataContext context)
        {
            Context = context;
            TextRepository = new TextRepository(Context);
            RatingRepository = new RatingRepository(Context);
        }
        public ITextRepository TextRepository { get; private set; }

        public IRatingRepository RatingRepository { get; private set; }

        public Task<int> SaveAsync()
        {
            return Context.SaveChangesAsync();
        }
    }
}
