﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Models
{
    public class TextModel
    {
        public int Id { get; set; }
        public string TextTitle { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public ICollection<int> RatingsIds { get; set; }
        public Guid AuthorId { get; set; }
    }
}
