﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Models
{
    public class UserIdModel
    {
        public Guid UserId { get; set; }
    }
}
