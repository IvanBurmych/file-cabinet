﻿using File_Cab.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Entities
{
    public class Rating
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public DateTime Date { get; set; }

        public Guid AuthorId { get; set; }
        public Text Text { get; set; }
    }
}
