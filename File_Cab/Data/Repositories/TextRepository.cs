﻿using File_Cab.Data;
using File_Cab.Entities;
using File_Cab.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Repositories
{
    public class TextRepository : Repository<Text>, ITextRepository
    {
        public TextRepository(DataContext context):base(context)
        {

        }
        public IQueryable<Text> GetAllWithRatingAndAuthorAsync()
        {
            return Context.Texts
                .Include(x => x.Ratings);
        }

        public Task<Text> GetWithRatingAndAuthorByIdAsync(int id)
        {
            return Context.Texts
                .Include(x => x.Ratings)
                .SingleOrDefaultAsync(a => a.Id == id);
        }
        public async Task AddRatingToText(Rating rating, int textId)
        {
            var text = await Context.Texts
                .Include(x => x.Ratings)
                .SingleOrDefaultAsync(a => a.Id == textId);
            text.Ratings.Add(rating);

            Context.Texts.Update(text);
        }
    }
}
