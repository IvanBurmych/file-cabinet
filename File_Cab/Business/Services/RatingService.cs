﻿using AutoMapper;
using File_Cab.Business.Interfaces;
using File_Cab.Business.Models;
using File_Cab.Entities;
using File_Cab.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Services
{
    public class RatingService : IRatingService
    {
        private readonly IUnitOfWork UoW;
        private readonly IMapper Mapper;
        public RatingService(IUnitOfWork uow, IMapper mapper)
        {
            UoW = uow;
            Mapper = mapper;
        }
        public async Task AddRatingAsync(RatingModel model)
        {
            var Rating = Mapper.Map<Rating>(model);
            await UoW.RatingRepository.AddAsync(Rating);
            await UoW.SaveAsync();
        }

        public async Task DeleteRatingByIdAsync(int modelId)
        {
            await UoW.RatingRepository.DeleteByIdAsync(modelId);
            await UoW.SaveAsync();
        }

        public IEnumerable<RatingModel> GetAllRatings()
        {
            var ratings = UoW.RatingRepository.GetAllWithTextAndAuthorAsync();
            List<RatingModel> models = new List<RatingModel>();

            foreach (var rating in ratings)
            {
                models.Add(Mapper.Map<RatingModel>(rating));
            }

            return models;
        }

        public async Task<RatingModel> GetRatingByIdAsync(int id)
        {
            var rating = await UoW.RatingRepository.GetWithTextAndAuthorByIdAsync(id);
            return Mapper.Map<RatingModel>(rating);
        }

        public async Task UpdateRatingAsync(RatingModel model)
        {
            var rating = Mapper.Map<Rating>(model);
            UoW.RatingRepository.Update(rating);
            await UoW.SaveAsync();
        }
    }
}
