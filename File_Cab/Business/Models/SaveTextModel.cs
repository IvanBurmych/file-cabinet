﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Models
{
    public class SaveTextModel
    {
        public string TextTitle { get; set; }
        public string Content { get; set; }
    }
}
