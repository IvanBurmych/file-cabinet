﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Models
{
    public class SaveRatingModel
    {
        public int TextId { get; set; }
        public string Comment { get; set; }
    }
}
