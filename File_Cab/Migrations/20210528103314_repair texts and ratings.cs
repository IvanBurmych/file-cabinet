﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace File_Cab.Migrations
{
    public partial class repairtextsandratings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Text",
                table: "Texts",
                newName: "Content");

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Texts",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Ratings",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "Texts");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "Ratings");

            migrationBuilder.RenameColumn(
                name: "Content",
                table: "Texts",
                newName: "Text");
        }
    }
}
