﻿using File_Cab.Business.Interfaces;
using File_Cab.Business.Models;
using File_Cab.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace File_Cab.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TextController : ControllerBase
    {
        private ITextService Service;
        private UserManager<AppUser> _userManager;
        public TextController(ITextService textService, UserManager<AppUser> userManager)
        {
            Service = textService;
            _userManager = userManager;

        }
        /// <summary>
        /// gives all texts 
        /// </summary>
        /// <returns>Texts</returns>
        [Authorize]
        [HttpGet("")]
        public ActionResult<IEnumerable<TextModel>> GetAllTexts()
        {
            return Ok(Service.GetAllTexts());
        }
        /// <summary>
        /// adds text 
        /// </summary>
        /// <param name="saveTextModel"></param>
        /// <returns>200</returns>
        [Authorize(Roles = "Admin, Moder")]
        [HttpPost("add")]
        public async Task<IActionResult> AddText(SaveTextModel saveTextModel)
        {
            if (saveTextModel.Content == null || saveTextModel.TextTitle ==null)
                return BadRequest();

            var authorId = _userManager.FindByNameAsync(HttpContext.User.Identity.Name).Result.Id;

            TextModel textModel = new TextModel
            {
                TextTitle = saveTextModel.TextTitle,
                Content = saveTextModel.Content,
                Date = DateTime.Now,
                AuthorId = authorId
            };
            await Service.AddTextAsync(textModel);

            return Ok();
        }
        /// <summary>
        /// adds rating
        /// </summary>
        /// <param name="saveRatingModel"></param>
        /// <returns>200</returns>
        [Authorize]
        [HttpPost("AddRating")]
        public async Task<IActionResult> AddRating(SaveRatingModel saveRatingModel)
        {
            if (saveRatingModel.Comment == null || saveRatingModel.Comment == "")
            {
                return BadRequest();
            }

            var authorId = _userManager.FindByNameAsync(HttpContext.User.Identity.Name).Result.Id;

            await Service.AddRating(saveRatingModel, authorId);
            return Ok();
        }
        /// <summary>
        /// search by key 
        /// </summary>
        /// <param name="word"></param>
        /// <returns>Texts</returns>
        [Authorize]
        [HttpGet("Search/{word}")]
        public ActionResult<IEnumerable<TextModel>> Search(string word)
        {
            var models = Service.SearchByWord(word);

            if (models == null)
                return NotFound();

            return Ok(models);
        }
        /// <summary>
        /// gives most popular texts
        /// </summary>
        /// <param name="count"></param>
        /// <returns>Texts</returns>
        [Authorize]
        [HttpGet("Popular/{count}")]
        public ActionResult<IEnumerable<TextModel>> GetMostPopularTexts(int count)
        {
            var models = Service.GetMostPopularTexts(count);

            if (models == null)
                return NotFound();

            return Ok(models);
        }
        /// <summary>
        /// gives texts by year
        /// </summary>
        /// <param name="year"></param>
        /// <returns>Texts</returns>
        [Authorize]
        [HttpGet("Year/{year}")]
        public ActionResult<IEnumerable<TextModel>> GetTextsByYear(int year)
        {
            if (year < 1900 || year > DateTime.Now.Year)
                return BadRequest();

            var models = Service.GetTextsByYear(year);

            if (models == null)
                return NotFound();

            return Ok(models);
        }
        /// <summary>
        /// gives texts by author id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>Texts</returns>
        [Authorize]
        [HttpPost("GetByAuthorId")]
        public ActionResult<IEnumerable<TextModel>> GetTextsByAuthorId(UserIdModel UserId)
        {
            var models = Service.GetTextsByAuthorId(UserId.UserId);

            if (models == null)
                return NotFound();

            return Ok(models);
        }
        /// <summary>
        /// gives ratings by text id
        /// </summary>
        /// <param name="TextId"></param>
        /// <returns>Ratings</returns>
        [Authorize]
        [HttpGet("Ratings/{TextId}")]
        public ActionResult<IEnumerable<RatingModel>> GetRatingsByTextId(int TextId)
        {
            var models = Service.GetRatingsByTextId(TextId);

            if (models == null)
                return NotFound();

            return Ok(models);
        }
        /// <summary>
        /// update text
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200</returns>
        [Authorize(Roles = "Admin, Moder")]
        [HttpPost("Update")]
        public async Task<IActionResult> Update(TextModel model)
        {
            if (model == null)
                return BadRequest();

            await Service.UpdateTextAsync(model);

            return Ok();
        }
        /// <summary>
        /// delete text
        /// </summary>
        /// <param name="id"></param>
        /// <returns>200</returns>
        [Authorize(Roles = "Admin, Moder")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await Service.DeleteTextByIdAsync(id);

            return Ok();
        }
    }
}
