﻿using File_Cab.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace File_Cab.Entities
{
    public class Text
    {
        public int Id { get; set; }
        public string TextTitle { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }

        [JsonIgnore]
        public ICollection<Rating> Ratings { get; set; }
        public Guid AuthorId { get; set; }
    }
}
