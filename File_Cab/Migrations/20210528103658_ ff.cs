﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace File_Cab.Migrations
{
    public partial class ff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Time",
                table: "Texts",
                newName: "TextTime");

            migrationBuilder.RenameColumn(
                name: "Time",
                table: "Ratings",
                newName: "RatingTime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TextTime",
                table: "Texts",
                newName: "Time");

            migrationBuilder.RenameColumn(
                name: "RatingTime",
                table: "Ratings",
                newName: "Time");
        }
    }
}
