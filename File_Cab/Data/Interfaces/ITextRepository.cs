﻿using File_Cab.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Repositories.Interfaces
{
    public interface ITextRepository : IRepository<Text>
    {
        IQueryable<Text> GetAllWithRatingAndAuthorAsync();
        Task<Text> GetWithRatingAndAuthorByIdAsync(int id);
        Task AddRatingToText(Rating rating, int textId);
    }
}
