﻿using File_Cab.Business.Models;
using File_Cab.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Interfaces
{
    public interface ITextService
    {
        IEnumerable<TextModel> GetAllTexts();
        IEnumerable<TextModel> SearchByWord(string word);
        Task<TextModel> GetTextByIdAsync(int id);
        Task AddTextAsync(TextModel model);
        Task UpdateTextAsync(TextModel model);
        Task DeleteTextByIdAsync(int modelId);
        IEnumerable<TextModel> GetMostPopularTexts(int count);
        IEnumerable<TextModel> GetTextsByAuthorId(Guid id);
        IEnumerable<TextModel> GetTextsByYear(int year);
        IEnumerable<RatingModel> GetRatingsByTextId(int id);
        Task AddRating(SaveRatingModel saveRatingModel, Guid authorId);
    }
}
