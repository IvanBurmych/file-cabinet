﻿using AutoMapper;
using File_Cab.Business.Models;
using File_Cab.Entities;
using File_Cab.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserAuthModel, AppUser>()
                .ForMember(u => u.UserName, opt => opt.MapFrom(ur => ur.Email));



            CreateMap<Rating, RatingModel>()
                .ForMember(u => u.TextId, opt => opt.MapFrom(ur => ur.Text.Id))
                .ReverseMap();

            CreateMap<Text, TextModel>()
                .ForMember(u => u.RatingsIds, opt => opt.MapFrom(ur => ur.Ratings.Select(x => x.Id)))
                .ReverseMap();
        }
    }
}
