﻿using File_Cab.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Interfaces
{
    public interface IRatingService
    {
        IEnumerable<RatingModel> GetAllRatings();
        Task<RatingModel> GetRatingByIdAsync(int id);
        Task AddRatingAsync(RatingModel model);
        Task UpdateRatingAsync(RatingModel model);
        Task DeleteRatingByIdAsync(int modelId);
    }
}
