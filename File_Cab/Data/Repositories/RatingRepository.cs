﻿using File_Cab.Data;
using File_Cab.Entities;
using File_Cab.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(DataContext context) : base(context)
        {

        }
        public IQueryable<Rating> GetAllWithTextAndAuthorAsync()
        {
            return Context.Ratings
                .Include(x => x.Text);
        }

        public Task<Rating> GetWithTextAndAuthorByIdAsync(int id)
        {
            return Context.Ratings
                .Include(x => x.Text)
                .SingleOrDefaultAsync(a => a.Id == id);
        }
    }
}
