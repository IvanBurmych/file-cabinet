﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Models
{
    public class RatingModel
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public DateTime Date { get; set; }
        public Guid AuthorId { get; set; }
        public int TextId { get; set; }
    }
}
