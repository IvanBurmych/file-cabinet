﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        ITextRepository TextRepository { get; }
        IRatingRepository RatingRepository { get; }
        Task<int> SaveAsync();
    }
}
