﻿using File_Cab.Entities;
using File_Cab.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace File_Cab.Data
{
    public class DataContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public DbSet<Text> Texts { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DataContext(DbContextOptions<DataContext> options)
        : base(options)
        {

        }
    }
}
