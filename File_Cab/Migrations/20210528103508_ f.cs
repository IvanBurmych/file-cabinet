﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace File_Cab.Migrations
{
    public partial class f : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Date",
                table: "Texts",
                newName: "Time");

            migrationBuilder.RenameColumn(
                name: "Date",
                table: "Ratings",
                newName: "Time");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Time",
                table: "Texts",
                newName: "Date");

            migrationBuilder.RenameColumn(
                name: "Time",
                table: "Ratings",
                newName: "Date");
        }
    }
}
