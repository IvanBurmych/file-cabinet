﻿using AutoMapper;
using File_Cab.Business.Interfaces;
using File_Cab.Business.Models;
using File_Cab.Entities;
using File_Cab.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace File_Cab.Business.Services
{
    public class TextService : ITextService
    {
        private readonly IUnitOfWork UoW;
        private readonly IMapper Mapper;
        public TextService(IUnitOfWork uow, IMapper mapper)
        {
            UoW = uow;
            Mapper = mapper;
        }

        public async Task AddTextAsync(TextModel model)
        {
            var text = Mapper.Map<Text>(model);
            await UoW.TextRepository.AddAsync(text);

            await UoW.SaveAsync();
        }

        public async Task DeleteTextByIdAsync(int modelId)
        {
            await UoW.TextRepository.DeleteByIdAsync(modelId);
            await UoW.SaveAsync();
        }

        public IEnumerable<TextModel> GetAllTexts()
        {
            var texts = UoW.TextRepository.GetAllWithRatingAndAuthorAsync();
            List<TextModel> models = new List<TextModel>();

            foreach (var text in texts)
            {
                models.Add(Mapper.Map<TextModel>(text));
            }

            return models;
        }

        public async Task<TextModel> GetTextByIdAsync(int id)
        {
            var text = await UoW.TextRepository.GetWithRatingAndAuthorByIdAsync(id);
            return Mapper.Map<TextModel>(text);
        }

        public IEnumerable<TextModel> SearchByWord(string word)
        {
            var texts = UoW.TextRepository.GetAllWithRatingAndAuthorAsync().Where(x => x.Content.Contains(word));

            List<TextModel> models = new List<TextModel>();

            foreach (var text in texts)
            {
                models.Add(Mapper.Map<TextModel>(text));
            }

            return models;
        }

        public IEnumerable<TextModel> GetTextsByYear(int year)
        {
            var texts = UoW.TextRepository.GetAllWithRatingAndAuthorAsync().Where(x => x.Date.Year == year);

            List<TextModel> models = new List<TextModel>();

            foreach (var text in texts)
            {
                models.Add(Mapper.Map<TextModel>(text));
            }

            return models;
        }

        public async Task UpdateTextAsync(TextModel model)
        {
            var text = Mapper.Map<Text>(model);
            UoW.TextRepository.Update(text);
            await UoW.SaveAsync();
        }

        public IEnumerable<TextModel> GetMostPopularTexts(int count)
        {
            var texts = UoW.TextRepository.GetAllWithRatingAndAuthorAsync().OrderByDescending(x => x.Ratings.Count).Take(count);

            List<TextModel> models = new List<TextModel>();

            foreach (var text in texts)
            {
                models.Add(Mapper.Map<TextModel>(text));
            }

            return models;
        }

        public IEnumerable<RatingModel> GetRatingsByTextId(int id)
        {
            var ratings = UoW.TextRepository.GetWithRatingAndAuthorByIdAsync(id).Result.Ratings;

            List<RatingModel> models = new List<RatingModel>();

            foreach (var rating in ratings)
            {
                models.Add(Mapper.Map<RatingModel>(rating));
            }

            return models;
        }

        public async Task AddRating(SaveRatingModel saveRatingModel, Guid authorId)
        {
            RatingModel ratingModel = new RatingModel
            {
                Comment = saveRatingModel.Comment,
                Date = DateTime.Now,
                AuthorId = authorId
            };

            var rating = Mapper.Map<Rating>(ratingModel);

            await UoW.TextRepository.AddRatingToText(rating, saveRatingModel.TextId);

            // UoW.TextRepository.GetWithRatingAndAuthorByIdAsync(saveRatingModel.TextId).Result.Ratings.Add(rating);

            await UoW.SaveAsync();
        }

        public IEnumerable<TextModel> GetTextsByAuthorId(Guid id)
        {
            var texts = UoW.TextRepository.GetAllWithRatingAndAuthorAsync().Where(x=>x.AuthorId == id);

            List<TextModel> models = new List<TextModel>();

            foreach (var text in texts)
            {
                models.Add(Mapper.Map<TextModel>(text));
            }

            return models;
        }
    }
}
