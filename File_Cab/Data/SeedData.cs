﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using File_Cab.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace File_Cab.Data
{
    public static class SeedData
    {
        public static async Task Init(IServiceProvider scopeServiceProvider)
        {
            var roleManager = scopeServiceProvider.GetService<RoleManager<AppRole>>();
            foreach (var roleName in RoleNames.AllRoles)
            {
                var role = roleManager.FindByNameAsync(roleName).Result;

                if (role == null)
                {
                    var res = roleManager.CreateAsync(new AppRole { Name = roleName }).Result;
                    if (!res.Succeeded)
                        throw new Exception(res.Errors.First().Description);
                }
            }


            var userManager = scopeServiceProvider.GetService<UserManager<AppUser>>();

            var adminResult = await userManager.CreateAsync(DefaultUsers.Administrator, "1qaz2wsx");
            var userResult = await userManager.CreateAsync(DefaultUsers.Moderator, "1qaz2wsx");
            var moderatorResult = await userManager.CreateAsync(DefaultUsers.User, "1qaz2wsx");

            userManager.Dispose();

            if (adminResult.Succeeded || userResult.Succeeded || moderatorResult.Succeeded)
            {
                var adminUser = userManager.Users.SingleOrDefault(x => x.UserName == DefaultUsers.Administrator.Email);
                var commUser = userManager.Users.SingleOrDefault(x => x.UserName == DefaultUsers.User.Email);
                var moderUser = userManager.Users.SingleOrDefault(x => x.UserName == DefaultUsers.Moderator.Email);

                await userManager.AddToRoleAsync(adminUser, RoleNames.Administrator);
                await userManager.AddToRoleAsync(commUser, RoleNames.User);
                await userManager.AddToRoleAsync(moderUser, RoleNames.Moderator);
 
            }
        }
    }
    public static class RoleNames
    {
        public const string Administrator = "Admin";
        public const string User = "User";
        public const string Moderator = "Moder";

        public static IEnumerable<string> AllRoles
        {
            get
            {
                yield return Administrator;
                yield return User;
                yield return Moderator;
            }
        }
    }
    public static class DefaultUsers
    {
        public static readonly AppUser Administrator = new AppUser { Email = "Administrator@goog.le", EmailConfirmed = true, UserName = "Administrator@goog.le" };
        public static readonly AppUser Moderator = new AppUser { Email = "Moderator@goog.le", EmailConfirmed = true, UserName = "Moderator@goog.le" };
        public static readonly AppUser User = new AppUser { Email = "User@goog.le", EmailConfirmed = true, UserName = "User@goog.le" };

        public static IEnumerable<AppUser> AllUsers
        {
            get
            {
                yield return Administrator;
                yield return User;
                yield return Moderator;
            }
        }
    }
}
