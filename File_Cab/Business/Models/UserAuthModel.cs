﻿using System;

namespace File_Cab.Models
{
    public class UserAuthModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
